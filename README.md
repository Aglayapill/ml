# ML

ML projects:

Pandas - using Pandas library to solve tasks

Crime - visualization data and hypothesis tests

Linear Regression - writing Linear Regression from scratch and comparing with sklearn implementation

Project work - preprocessing data and predicting heart attack using Logistic Regression, SGDClassifier, Decision Tree

Neuron 1, 2 - writing Neuron class with different activation functions

NN - working with Neural Networks(PyTorch) to classify Fashionmnist dataset
